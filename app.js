var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.locals.pretty = true;
app.set('view engine', 'jade');
app.set('views', './views');
app.use(express.static('images'));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/form', (req, res) => {
    res.render('form');
});
app.get('/form_receiver', (req, res) => {
    var title = req.query.title;
    var description = req.query.description;
    res.send(title+','+description);
});
app.post('/form_receiver', (req, res) => {
    var title = req.body.title;
    var description = req.body.description;
    res.send(title+','+description);
});

// Non-Sementic URL
// app.get('/topic', (req, res) => {
//     //?id=1&name=jin
//     //res.send(req.query.id+','+req.query.name);
//     var topics = [
//         'Javascripot is ....',
//         'Nodejs is....',
//         'Express is .....'
//     ];
//     var output = `
//         <a href="/topic?id=0">Javascript</a><br>
//         <a href="/topic?id=1">Nodejs</a><br>
//         <a href="/topic?id=2">Expressjs</a><br>
//         ${topics[req.query.id]}
//     `;
//     res.send(output);
// });

// Sementic URL
app.get('/topic/:id', (req, res) => {
    //?id=1&name=jin
    //res.send(req.query.id+','+req.query.name);
    var topics = [
        'Javascripot is ....',
        'Nodejs is....',
        'Express is .....'
    ];
    var output = `
        <a href="/topic/0">Javascript</a><br>
        <a href="/topic/1">Nodejs</a><br>
        <a href="/topic/2">Expressjs</a><br>
        ${topics[req.params.id]}
    `;
    res.send(output);
});
app.get('/topic/:id/:mode', (req, res) => {
    res.send(req.params.id +', '+ req.params.mode);
});
app.get('/template', (req, res) => {
    res.render('temp', {time:Date(), title:'Jade'});
});
app.get('/', function(req, res){
    res.send('Hello home page');
});
app.get('/dynamic', (req, res) => {
    var lis = '';
    for (var i = 0; i < 5; i++) {
        lis = lis + '<li>coding</li>';
    }
    var time = Date();
    var output= `<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8" />
            <title>haha</title>
        </head>
        <body>
            Hello, Dynamic!
            <ul>
                ${lis}
            </ul>
            ${time}
        </body>
    </html>`;
    res.send(output);
});
app.get('/route', (req, res) => {
    res.send('Hello route, <img src="/chicken.jpg">');
});
app.get('/login', (req,res) => {
    res.send('<h1>Login please</h1>');
});
app.listen(3000, function(){
    console.log('Conneted port 3000!');
});
