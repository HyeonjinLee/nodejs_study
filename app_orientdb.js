var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var app = express();

var OrientDB = require('orientjs');
var server = OrientDB({
  host: 'localhost',
  port: 2424,
  username: 'root',
  password: 'abc4343'
});
var db = server.use('o2');
app.use(bodyParser.urlencoded({ extended: false}));
app.locals.pretty = true;
app.set('views', './views_orientdb');
app.set('view engine', 'jade');
app.get('/topic/new', function(req, res) {
    console.log('enter');
    res.render('new1');
});
app.get('/topics/new', function(req, res) {
    fs.readdir('data', (err, files) => {
        if(err) {
            console.log(err);
            res.status(500).send('Internal Server Error');
        }
        res.render('new', {topics: files});
    });
});
app.get(['/topic','/topic/:id' ], (req, res) => {
    var sql = 'SELECT FROM topic';
    db.query(sql).then(function(topics) {
       res.render('view', {topics: topics});
    });


    // fs.readdir('data', (err, files) => {
    //     if(err) {
    //         console.log(err);
    //         res.status(500).send('Internal Server Error');
    //     }
    //     var id = req.params.id;
    //     if (id) {
    //         // exist id
    //         fs.readFile('data/'+id, 'utf8', function(err, data){
    //             if(err){
    //                 res.status(500).send('Internal Server Error');
    //             }
    //             res.render('view', {topics:files, title:id, description:data});
    //         });
    //     } else {
    //         // no exist id
    //         res.render('view', {topics:files, title:'Welcome', description:'Hello, Javascript for desc'});
    //     }
    // });
});
//remove dupulication
// app.get('/topic/:id', (req, res)=>{
//     var id = req.params.id;
//     fs.readdir('data', (err, files) => {
//         if(err) {
//             console.log(err);
//             res.status(500).send('Internal Server Error');
//         }
//         fs.readFile('data/'+id, 'utf8', function(err, data){
//             if(err){
//                 res.status(500).send('Internal Server Error');
//             }
//             res.render('view', {topics:files, title:id, description:data});
//         });
//     });
// });
app.post('/topic', function(req, res) {
    var title = req.body.title;
    var description = req.body.description;
    console.log(description);
    fs.writeFile('data/'+ title, description, function(err){
        if(err){
            console.log(err);
            res.status(500).send('Internal Server Error');
        }
        res.redirect('/topic/'+title);
    });
});
app.listen(3000, function() {
    console.log('Connected, 3000 port!');
});
